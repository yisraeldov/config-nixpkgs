* Nix Home configuration

This is the configuration for [[https://nix-community.github.io/home-manager/][home-manager]], NixOs configuration is in
~/etc/nixos/~


** Installation 

*** Install Nix

*** Install Home Manager

*** Clone this repo
    
*** Home manager Switch

    
** Installation on New Mac

   
*** Set up macbook minimally. 
This is the setup procedure on MacBook Air 2020 M1 
**** Choose language
     Best to choose English for now.
**** Select Region
     Use the default
**** Skip Accessibility for now
     (You can turn on dark-mode though)
**** Connect to Wifi
**** Migration Assistant
     Click ~Not Now~, we want to do a fresh install.
     It is hidden on the lower left corner.
**** Set up apple ID
     This should be optional. 
**** Terms and conditions
     Agree or not.
**** Create a Computer account
**** "Setting Up Account..." This may take a while
**** iCloud Keychain?
     No thanks.
**** Lots of opt-ins
**** Turn on Fille Vault disk Encryption
     This will come into play later and make our lives a bit more
     difficult but nothing we can't get around.
**** Touch ID
**** Apple Pay
     find the link for "setup later"
**** Open a terminal
     ~command + space~ and type ~terminal~
     Make it full screen with the green ~+~
**** Install rosetta on M1
     #+begin_src bash
       /usr/sbin/softwareupdate --install-rosetta
     #+end_src
*** Install Nix
    Use the recommended installation method in the [[https://nixos.org/manual/nix/stable/#sect-macos-installatio][nix manual]] for mac.
    #+begin_src bash
      sh <(curl -L https://nixos.org/nix/install) \
          --darwin-use-unencrypted-nix-store-volume
    #+end_src
    This is not actually unencrypted, and it only stores binary data.
    
    You will be prompted to enter your password
    Follow the instructions to add the correct line to your ~.profile~
**** Add the unstable channel
     The installer may have installed the unstable channel by default
     but we want to explicitly use it

     #+begin_src bash
       nix-channel --add  https://nixos.org/channels/nixos-unstable nixos-unstable
       nix-channel --update
     #+end_src
**** Test that nix installed
     Source your ~.profile~ or open a new terminal

     Install and run the *hello* package
     #+begin_src bash
       nix-env -iA nixpkgs.hello
       hello
       # Lets test what arch this was built for
       file `which hello`
     #+end_src
     
     if everything was successful you should see that it was built
     for ~arm64~.

     Now lets remove the package.
     #+begin_src bash
       nix-env -e nixpkgs.hello
     #+end_src

**** M1 mac issues
     So nix has support for M1 but many of the packages don't yet, so
     you need to force it to use x86 over rosetta.

     Add the following to ~/etc/nix/nix.config~

     #+begin_src nix
       system = x86_64-darwin
       # system = aarch64-darwin
       extra-platforms = x86_64-darwin aarch64-darwin
     #+end_src

     see [[https://github.com/LnL7/nix-darwin/issues/334][this issue for possibly better solutions]]
*** Install Nix-Darwin

    https://daiderd.com/nix-darwin/

**** Use nix to install nix-darwin
     #+begin_src bash
       #download and build the installer
       nix-build https://github.com/LnL7/nix-darwin/archive/master.tar.gz\
                 -A installer
       #run the installer
       ./result/bin/0darwin-installer
     #+end_src

     You will be asked if you want to edit the default configuration,
     say yes so you can remove vim 🤢

     (you can quit nano with ~ctl-x~)

     Answer *yes* to all the remaining questions.

     Open a new terminal window (cmd-N)
**** Test that it installed correctly
     #+begin_src bash
     darwin-rebuild edit
     #+end_src

     That should open your config up in *nano* (don't worry will add
     emacs soon).

     Edit the file and add ~pkgs.hello~ inside of the
     ~environment.systemPackages~ list. Quit and save

     Then rebuild the darwin install
     #+begin_src bash
       darwin-rebuild switch
       # lots of text ...
       file `which hello`
     #+end_src

     You maybe tempted to install more packages, but don't do that
     just yet.

**** Cleanup
     #+begin_src bash
       #remove the darwin installer
       rm -rv result
     #+end_src
*** Install Home Manager
    We are going to install [[https://nix-community.github.io/home-manager/index.html#sec-install-nix-darwin-module][home-manager using nix-darwin]]
**** Add the home-manager channel

     #+begin_src bash
       nix-channel --add \
                   https://github.com/nix-community/home-manager/archive/master.tar.gz\
                   home-manager
       nix-channel --update
     #+end_src
**** Edit your darwin config
     Add after the second ~{~, most likely the *4th line*:
     #+begin_src nix
       imports = [ <home-manager/nix-darwin> ];

       #specify your user in darwin
       #this is not in the home-manger documentation but is important
       users.users.yisraeldov = rec {
         name = yisraeldov;
         home = /Users/yisraeldov/ ;
       }
       #home manager config for users
       #change to your userename
       home-manager.users.yisraeldov = { pkgs, ... }: {
         home.packages = [ pkgs.git ];
         programs.zsh.enable = true;
       };


       #This saves an extra Nixpkgs evaluation, adds consistency, and removes
       #the dependency on NIX_PATH, which is otherwise used for importing
       #Nixpkgs.

       home-manager.useGlobalPkgs = true;
     #+end_src

     Then build the nix darwin
     #+begin_src bash
       darwin-rebuild switch
     #+end_src
*** Clone this repo
    Since we don't have ssh keys set up yet we will use https

    #+begin_src bash
      mv ~/.nixpkgs/ ~/.nixpkgs.old
      git clone https://gitlab.com/yisraeldov/config-nixpkgs.git ~/.nixpkgs
    #+end_src

    We can now change our darwin-config to reference the ~home.nix~
    from our repo

    #+begin_src nix
        home-manager.users.yisraeldov = import ./home.nix;
    #+end_src
*** Home manager Switch
    Since we are using darwin to manage home-manager we can use
    ~darwin-rebuild switch~ to enable our homemanger config.
*** TODO Setting up GPG/SSH keys
    Mac can't use the gpg set up that we where using on linux.
    Add to ~darwin-configuration.nix~
    #+begin_src nix 
      programs.gnupg.agent = {
        enable = true;
        enableSSHSupport = true;
      };

    #+end_src

    after a rebuild of darwin you should have ~gpg~ installed.
**** Possibly fix the permissions
     You may have an issue with the permissions on your gpg dir.

     #+begin_src bash
       chmod 600 ~/.gnupg/*
       chmod 700 ~/.gnupg
     #+end_src
     
**** Generate a new key
     #+begin_src bash
       gpg --generate-key
     #+end_src
     And then follow the instructions.

     You will be prompted to enter a password to secure your key via
     the gui.
**** Add a sub key for authentication.
     This is optional
**** Add your key to ssh

     Find the *keygrip* of the key you want to use:
     #+begin_src bash
       gpg2 --with-keygrip -k alice
     #+end_src

     Then add it to your home.nix followed by the TTL

     #+begin_src nix
  home.file.gpgSshKeys = {
    target = ".gnupg/sshcontrol";
    text = ''
      8B747AB47BFCB37CCAD193C28E41247E85E36F6E 600
                                               '';
  };
     #+end_src
