{ config, pkgs, ... }:
with builtins;
with import <nixpkgs> { };

let
  unstable = import <nixos-unstable> { };
  command-not-found-db = pkgs.callPackage ./command-not-found-db { };
  hostname = (if lib.pathExists /etc/hostname then
    lib.fileContents /etc/hostname
  else
    "no-hostname");
in {
  # nixpkgs.config.allowlistedLicenses = with lib.licenses;
  #   [ unfreeRedistributable ];
  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "slack"
      "steam"
      "font-bh-lucidatypewriter"
      "nomachine-client"
      "geekbench"
      "nvidia-x11"
      "unigine-valley"
    ];

  imports = [ ./linux-desktop.nix ./games.nix ]
    ++ (if stdenv.isDarwin then [ ./practitest.nix ] else [ ])
    ++ (let hostConfigPath = lib.debug.traceVal (./. + "/${hostname}.nix");
    in (if lib.pathExists hostConfigPath then [ hostConfigPath ] else [ ]));
  linuxDesktop.enable = stdenv.isLinux;
  home.packages = with pkgs; [
    unzip
    htop
    fd
    bat
    ripgrep
    bash-completion
    nix-zsh-completions
    wakatime
    nixfmt
    nerdfonts
    lsd
    git-annex
    git-crypt
    pandoc
    rclone
    culmus # hebrew fonts

    #command-not-found-db
    imagemagick
    exercism
    element-desktop
    unstable.dino
    #    (unstable.psi-plus.override (oldAttrs: rec { enablePsiMedia = true; }))
    python39Packages.pygpgme
    python39Packages.gpgme

    inkscape
    blender

    foliate # ebook reader
    imagemagick
    ffmpeg-full
    unstable.mpv

    unstable.youtube-dl
    unstable.yt-dlp
    unstable.tartube-yt-dlp
    unstable.btop

    aspell
    aspellDicts.yi
    aspellDicts.he
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science
    #nyxt
    browserpass
    emacsPackages.emacsql
    libvterm
    libnotify

    (ulauncher.overrideAttrs (oldAttrs: {
      #this is all needed for plugins.
      propagatedBuildInputs = with python39Packages;
        [ fzf fd pytz tzlocal dateparser python-gitlab python-frontmatter ]
        ++ oldAttrs.propagatedBuildInputs;
    }))

    geekbench
    glmark2
    #    unigine-valley
    phoronix-test-suite
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.command-not-found.enable = true;
  #programs.command-not-found.dbPath = "${command-not-found-db}/programs.sqlite";

  programs.direnv.enable = true;
  programs.direnv.enableZshIntegration = true;
  programs.direnv.nix-direnv.enable = true;

  programs.chromium = {
    extensions = [
      { id = "eljmjmgjkbmpmfljlmklcfineebidmlo"; } # psono
      { id = "eimadpbcbfnmbkopoojfekhnkhdbieeh"; } # dark reader
      { id = "kofmhmemalhemmpkfjhjfkkhifonoann"; } # watch on odysee
      { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock origin
      { id = "naepdomgkenhinolocfifgehidddafch"; } # browser pass
    ];
  };

  programs.mpv = {
    enable = true;
    package = unstable.mpv;
    config = {
      speed = 1.8;
      save-position-on-quit = true;
      window-maximized = true;
      profile = "gpu-hq";
      x11-bypass-compositor = true;
      vo = "xv";
      vd-lavc-threads = 4;
    };
  };

  home.sessionVariables = rec {
    EDITOR = "emacsclient -c";
    SHELL = "${pkgs.zsh}/bin/zsh";
    QT_STYLE_OVERRIDE = "Adwaita-Dark";
    QT_QPA_PLATFORMTHEME = "gtk2";
  };

  home.shellAliases = {
    mic-check = " ffmpeg -f pulse -i default  -f matroska  - | ffplay - ";
  };

  # email
  accounts.email.accounts = {
    lebowtech = {
      primary = lib.mkDefault true;
      realName = "Yisrael Dov Lebow";
      address = "lebow@lebowtech.com";
      aliases = [ "lebow@lebowtech.co.il" ];
      gpg.signByDefault = true;
      gpg.key = "defbcfd001b445c1c3751ca06ae703b3e171c686";
      imap.host = "mail.dreamhost.com";
      smtp.host = "mail.dreamhost.com";
      userName = "lebow@lebowtech.com";
      passwordCommand = "pass lebow@lebowtech.com";
      mbsync.enable = true;
      mbsync.create = "both";
      mbsync.expunge = "none";
    };
    gmail = {
      primary = false;
      realName = "Yisrael Dov Lebow";
      address = "yisraldov@gmail.com";
      gpg.signByDefault = true;
      gpg.key = "defbcfd001b445c1c3751ca06ae703b3e171c686";
      flavor = "gmail.com";
      imap.host = "imap.gmail.com";
      smtp.host = "smtp.gmail.com";
      userName = "yisraeldov@gmail.com";
      passwordCommand = "pass yisraeldov@gmail.com";
      mbsync.enable = false;
      mbsync.patterns = [ "Inbox" "Sent Messages" ];
      mbsync.create = "maildir";
      mbsync.expunge = "maildir";
    };

  };

  programs.mbsync.enable = true;
  programs.mu.enable = true;

  programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (exts: [ exts.pass-otp ]);
  };
  programs.browserpass.enable = true;

  programs.bash = {
    enable = true;
    initExtra = ''
       export NIX_PATH=$NIX_PATH:$HOME/.nix-defexpr/channels
        Use bash-completion, if available
      . ${pkgs.bash-completion}/etc/profile.d/bash_completion.sh
    '';
  };

  programs.lsd = {
    enable = true;
    enableAliases = true;
    settings.date = "relative";
  };

  programs.git = {
    enable = true;
    package = lib.mkDefault pkgs.gitFull;
    signing.signByDefault = true;
    signing.key = "lebow@lebowtech.com";
    userEmail = "lebow@lebowtech.com";
    userName = "Yisrael Dov Lebow";
    ignores = [ "*~" "\\#*\\#" ".\\#*" ];
    delta.enable = true; # fancy diff
    delta.options.side-by-side = false;
    extraConfig = {
      push.recurseSubmodules = "check";
      diff.submodule = "log";
      fetch.parallel = 0;
      status.submoduleSummary = 5;
    };
  };

  programs.emacs.enable = true;

  programs.gpg.enable = true;

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    defaultKeymap = "emacs";
    envExtra = ''
      [ -f /etc/profile.d/nix.sh ] && . /etc/profile.d/nix.sh
      #export NIX_PATH=$NIX_PATH:$HOME/.nix-defexpr/channels
      #this is for nix on guest os with home manager
      export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels:$NIX_PATH
      export SSH_AUTH_SOCK=$(${pkgs.gnupg}/bin/gpgconf --list-dirs agent-ssh-socket)
    '';
  };

  programs.starship.enable = true;
  programs.starship.enableZshIntegration = true;
  programs.starship.settings = {
    character = {
      success_symbol = ">";
      error_symbol = "[!](bold red)";
    };
  };
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "yisraeldov";
  home.homeDirectory = lib.mkIf (stdenv.isLinux) "/home/yisraeldov";
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}
