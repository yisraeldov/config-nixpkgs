{ config, pkgs, lib, ... }:
with lib;
let unstable = import <nixos-unstable> { };
in {
  home.packages = with pkgs;
    [
      xonotic
      (retroarch.override { cores = with libretro; [ citra snes9x ]; })
    ] ++ (if stdenv.isLinux then
    # Linix only games
    [
      unstable.openttd
      lincity
      unstable.hedgewars
      superTuxKart
      superTux
      # torcs                
      sixpair
      jstest-gtk
      antimicrox
    ] else
      [ ]);
}
