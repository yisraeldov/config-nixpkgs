{ config, pkgs, ... }:
let
  pkgsM1 = import <nixpkgs> { localSystem = "aarch64-darwin"; };
in
{
  imports = [ <home-manager/nix-darwin> ];

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = [ pkgsM1.nano
                                 pkgsM1.gnupg
                                 pkgs.yabai
                                 (pkgs.callPackage ./apps/steam { })
                                 (pkgs.callPackage ./apps/ungoogled-chromium { })
                                 (pkgs.callPackage ./apps/barrier { })];

  nixpkgs.config.allowUnfree = true;
  users.users.yisraeldov = {
    name = "yisraeldov";
    home = /Users/yisraeldov;
  };

  #if you use global packages pkgs won't be availabe to home.nix
  home-manager.useGlobalPkgs = false;
  home-manager.users.yisraeldov = import ./home.nix ;

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # Auto upgrade nix package and the daemon service.
  # services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;

  # Create /etc/bashrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  # programs.fish.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  services.skhd.enable = true ;

  networking.hostName = "ydl-air";
  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
  system.keyboard.enableKeyMapping = true;
  #lower is faster
  system.defaults.NSGlobalDomain.KeyRepeat = 1;
  #system.keyboard.nonUS.remapTilde = true;
  system.keyboard.remapCapsLockToControl = true;
  system.defaults.finder.AppleShowAllExtensions = true;
}
