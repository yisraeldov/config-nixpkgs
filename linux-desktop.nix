{ config, pkgs, lib, ... }:
with lib;
let
  unstable = import <nixos-unstable> { };
  pidgin-gpg = pkgs.callPackage ./pidgin-gpg.nix { };
in {
  options = {
    linuxDesktop.enable = mkEnableOption "Enable linux desktop apps";
  };

  config = mkIf config.linuxDesktop.enable {
    #    xsession.enable = true;
    #    xsession.windowManager.command = ''
    #   "${pkgs.cinnamon.cinnamon-session}/bin/cinnamon-session"
    # '';

    #    xsession.initExtra = "${pkgs.ulauncher}/bin/ulauncher";
    home.packages = with pkgs;
      mkIf config.linuxDesktop.enable [
        # ungoogled-chromium
        brave
        clementine
        electrum # bitcoin wallet
        #etcher
        gnome.gnome-boxes
        gnome.gnome-calendar
        gnome3.cheese
        gnome3.vinagre
        gnupg
        kdeconnect
        linphone
        numberstation
        obs-studio
        pkgs.finger_bsd
        pkgs.libsForQt5.qtstyleplugins
        python38
        qbittorrent
        qemu_kvm
        rhythmbox
        paprefs
        shutter
        #        unstable.deltachat-electron
        unstable.lbry
        unstable.nheko
        xournalpp
        # (python38Packages.py3status.overrideAttrs (oldAttrs: {
        #   propagatedBuildInputs = with python38Packages;
        #     [ pytz tzlocal ] ++ oldAttrs.propagatedBuildInputs;
        # }))
        libgtop
        syncthingtray
        simple-scan
        rclone-browser
        unstable.briar-desktop
      ];
    qt = {
      enable = true;
      platformTheme = "gtk";
      style = {
        name = "adwaita-dark";
        package = pkgs.adwaita-qt;
      };
    };

    programs.pidgin = {
      enable = true;
      plugins = [
        pkgs.pidgin-otr
        pkgs.pidgin-osd
        pkgs.pidgin-window-merge
        pkgs.purple-plugin-pack
        pkgs.purple-hangouts
        pkgs.purple-matrix
        pkgs.purple-lurch
        pkgs.pidgin-xmpp-receipts
        pkgs.pidgin-opensteamworks
        pkgs.pidgin-carbons
        pkgs.purple-xmpp-http-upload
        pkgs.purple-discord
        #pidgin-gpg
      ];
    };

    programs.bash.enableVteIntegration = true;

    # TODO: Find a way to enable darkmode brave://flags/#enable-force-dark
    programs.chromium = {
      enable = true;
      package = pkgs.brave;
    };

    xdg.enable = true;

    services.emacs = {
      enable = false;
      client.enable = true;
    };

    services.imapnotify.enable = true;

    systemd.user.services.emacs.Unit = {
      After = [ "graphical-session.target" ];
    };

    services.password-store-sync.enable = true;
    services.gpg-agent = {
      verbose = true;
      enable = true;
      enableSshSupport = true;
      enableExtraSocket = true;
      sshKeys = [
        "5F6ECFD9AE6B412D8587C64F91A7B8B663F63AD3"
        "rsa4096/6AE703B3E171C686"
      ];
      defaultCacheTtl = 24 * 30 * 60 * 60;
      maxCacheTtl = 24 * 30 * 60 * 60;
    };
    services.kdeconnect.enable = true;
    services.kdeconnect.indicator = true;

    services.redshift = {
      enable = true;
      tray = true;
      dawnTime = "4:00-5:00";
      duskTime = "17:00-17:30";
    };
    services.pantalaimon = {
      enable = true;
      settings = {
        Default = {
          LogLevel = "Debug";
          SSL = false;
        };
        matrix-org = { Homeserver = "https://matrix.org"; };
      };
    };
  };
}
