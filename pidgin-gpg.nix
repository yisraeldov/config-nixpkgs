{ lib, stdenv, fetchFromGitHub, pidgin, autoreconfHook269, pkg-config, gpgme }:

stdenv.mkDerivation rec {
  pname = "pidign-gpg";
  version = "0.5";

  src = fetchFromGitHub {
    owner = "segler-alex";
    repo = "pidgin-gpg";
    rev = "${version}";
    sha256 = "0j3kczw7l8hprvpal20ms449srf69752r8ypixan40xwqj6dbky2";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ ];
  buildInputs =
    [ pidgin gpgme  autoreconfHook269 pkg-config];

  dontUseCmakeConfigure = true;

  installPhase = ''
    install -Dm755 -t $out/lib/purple-2 build/pdigin_gpg.so
  '';

  meta = with lib; {
    homepage = "https://github.com/segler-alex/pidgin-gpg";
    description = "GPG libpurple";
    license = licenses.gpl3;
    platforms = platforms.linux;
    maintainers = with maintainers; [ ];
  };
}
